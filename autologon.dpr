program autologin;

{$APPTYPE CONSOLE}

uses
  SysUtils, Registry, Windows;

const VERSION = '0.1';

var Reg           : TRegistry;
    Action,
    S,
    UserName,
    UserPassword,
    ReUserPassword,
    DomainName    : String;
    hConsoleOut,
    hConsoleInput : THandle;
    IBuff         : TInputRecord;
    IEvent        : DWord;

function ParExist(AParShortName, AParName: String): Boolean;
var i: Integer;
begin
  for i:=1 to ParamCount do begin
    if (Copy(LowerCase(ParamStr(i)), 0, Length(AParShortName)+1) = '-'+AParShortName) or
    (Copy(LowerCase(ParamStr(i)), 0, Length(AParName)+2) = '--'+AParName) then begin
      Result := True;
      Exit;
    end;
  end;
  Result := False;
end;

function ParValue(AParShortName, AParName: String): String;
var i: Integer;
begin
  Result := '';
  for i:=1 to ParamCount do begin
    if (Copy(LowerCase(ParamStr(i)), 0, Length(AParShortName)+1) = '-'+AParShortName) or
    (Copy(LowerCase(ParamStr(i)), 0, Length(AParName)+2) = '--'+AParName) then begin
      if Pos('=', ParamStr(i)) > 0 then
        Result := Copy(ParamStr(i), Pos('=', ParamStr(i))+1, Length(ParamStr(i)));
      Exit;
    end;
  end;
end;

function RebootWorkStation: Boolean;
var TTokenHd: THandle;
    TTokenPvg: TTokenPrivileges;
    cbtpPrevious: DWORD;
    rTTokenPvg: TTokenPrivileges;
    pcbtpPreviousRequired: DWORD;
    tpResult: Boolean;
const
  SE_SHUTDOWN_NAME = 'SeShutdownPrivilege';
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then begin
    tpResult := OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES or TOKEN_QUERY, TTokenHd);
    if tpResult then begin
      tpResult := LookupPrivilegeValue(nil, SE_SHUTDOWN_NAME, TTokenPvg.Privileges[0].Luid);
      TTokenPvg.PrivilegeCount := 1;
      TTokenPvg.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;
      cbtpPrevious := SizeOf(rTTokenPvg);
      pcbtpPreviousRequired := 0;
      if tpResult then
        Windows.AdjustTokenPrivileges(TTokenHd, False, TTokenPvg, cbtpPrevious, rTTokenPvg, pcbtpPreviousRequired);
    end;
  end;
  Result := ExitWindowsEx(EWX_REBOOT or EWX_FORCE, 0);
end;

function LockWorkStation: Boolean;
type TLockWorkStation = function: Boolean;
var hUser32: HMODULE;
    LockWorkStat: TLockWorkStation;
begin
  Result := False;
  hUser32 := GetModuleHandle('USER32.DLL');
  if hUser32 <> 0 then begin
    @LockWorkStat := GetProcAddress(hUser32, 'LockWorkStation');
    if @LockWorkStat <> nil then begin
     LockWorkStat;
     Result := True;
    end;
  end;
end;

procedure GotoXY(X, Y: Word);
var Cord: TCoord;
begin
  Cord.X := X;
  Cord.Y := Y;
  SetConsoleCursorPosition(hConsoleOut, Cord);
end;

procedure SetAttr(attr: word);
begin
  SetConsoleTextAttribute(hConsoleOut, attr);
end;

procedure Cls;
var ConsoleInfo: TConsoleScreenBufferInfo;
    Coord:TCoord;
    WrittenChars:DWORD;
begin
  FillChar(ConsoleInfo,SizeOf(TConsoleScreenBufferInfo),0);
  FillChar(Coord,SizeOf(TCoord),0);
  GetConsoleScreenBufferInfo(hConsoleOut, ConsoleInfo);
  FillConsoleOutputCharacter(hConsoleOut,' ', ConsoleInfo.dwSize.X * ConsoleInfo.dwSize.Y, Coord, WrittenChars);
  GotoXY(0,0);
end;

function ReadPassword: String;
var Continue: Boolean;

  procedure ClrBackChar;
  var ConsoleInfo: TConsoleScreenBufferInfo;
  begin
    GetConsoleScreenBufferInfo(hConsoleOut, ConsoleInfo);
    GotoXY(ConsoleInfo.dwCursorPosition.X-1, ConsoleInfo.dwCursorPosition.Y);
    Write(' ');
    GotoXY(ConsoleInfo.dwCursorPosition.X-1, ConsoleInfo.dwCursorPosition.Y);
  end;

begin
  Continue := True;
  Result   := '';
  while Continue do begin
    ReadConsoleInput(hConsoleInput, IBuff, 1, IEvent);
    if IBuff.EventType = KEY_EVENT then begin
      if (IBuff.Event.KeyEvent.bKeyDown = True) then begin
        if (IBuff.Event.KeyEvent.wVirtualKeyCode = VK_RETURN) then begin
          WriteLn;
          Continue := False;
        end else if (IBuff.Event.KeyEvent.wVirtualKeyCode = VK_BACK) then begin
          if Length(Result) > 0 then begin
            Result := Copy(Result,0,Length(Result)-1);
            ClrBackChar;
          end;
        end else if IBuff.Event.KeyEvent.AsciiChar > #0 then begin
          Result := Result + IBuff.Event.KeyEvent.AsciiChar;
          Write('*');
        end;
      end;
    end;
  end;
end;

procedure EnableAutoLogin;
begin
  try
    if (Pos('@', UserName) > 0) then begin
      DomainName := Copy(UserName,Pos('@', UserName)+1, Length(UserName));
      UserName   := Copy(UserName,0, Pos('@', UserName)-1);
    end;
    UserName := Trim(UserName);
    if UserName = '' then begin
      Write('Enable auto logon: ');
      SetAttr(12);
      WriteLn('FALSE');
      SetAttr(7);
      Exit;
    end;
    Reg.OpenKey('SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon',False);
    Reg.WriteString('AutoAdminLogon','1');
    Reg.WriteString('DefaultUserName',UserName);
    Reg.WriteString('DefaultDomainName',DomainName);
    Reg.WriteString('DefaultPassword',UserPassword);
    Reg.CloseKey;
    Write('Enable auto logon: ');
    SetAttr(10);
    WriteLn('OK');
  except begin
    Write('Enable auto logon: ');
    SetAttr(12);
    WriteLn('FALSE');
  end;
  end;
  SetAttr(7);
end;

procedure DisableAutoLogon;
begin
  try
    Reg.OpenKey('SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon',False);
    Reg.WriteString('AutoAdminLogon','0');
    if Reg.ValueExists('DefaultPassword') then
      Reg.DeleteValue('DefaultPassword');
    Reg.CloseKey;
    Write('Disable auto logon: ');
    SetAttr(10);
    WriteLn('OK');
  except begin
    Write('Disable auto logon: ');
    SetAttr(12);
    WriteLn('FALSE');
  end;
  end;
  SetAttr(7);
end;

procedure SmartReboot;
begin
  Reg.OpenKey('SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce',False);
  if ParExist('l','lock') then
    Reg.WriteString('LockWorkStation',ParamStr(0)+' disable -l')
  else
    Reg.WriteString('LockWorkStation',ParamStr(0)+' disable');
  Reg.CloseKey;
  RebootWorkStation;
end;

procedure SetAutoLock(AValue: Boolean);
begin
  try
    Reg.OpenKey('SOFTWARE\Microsoft\Windows\CurrentVersion\Run',True);
    if AValue then
      Reg.WriteString('AutoLockOnBoot',ParamStr(0)+' -l')
    else
      if Reg.ValueExists('AutoLockOnBoot') then
        Reg.DeleteValue('AutoLockOnBoot');
    Reg.CloseKey;
    Write('Auto lock workstation: ');
    SetAttr(10);
    if AValue then
      WriteLn('ON')
    else
      WriteLn('OFF');
  except begin
    Write('Auto lock workstation: ');
    SetAttr(12);
    WriteLn('FALSE');
  end;
  end;
  SetAttr(7);
end;

procedure Help;
begin
  WriteLn('Auto logon v ' + VERSION+' [http://www.it2k.ru]');
  WriteLn('Usage: '+ParamStr(0)+' [keys] ... [options]');
  WriteLn('');
  WriteLn('keys    [enable] enable auto logon with options -u and -p');
  WriteLn('                 If use options -sr then set options lock');
  WriteLn('                 workstation on boot and reboot system.');
  WriteLn('        [disable] disable auto logon');
  WriteLn('        [autolock] set auto lock workstation on boot.');
  WriteLn('        [noautolock] clear auto lock workstation on boot.');
  WriteLn('');
  WriteLn('options [-u or --user] set username for auto logon if domain use [user@domain]');
  WriteLn('        [-p or --password] set password for auto logon');
  WriteLn('        [-l or --lock] lock workstation');
end;

begin
  SetConsoleTitle('Auto logon v'+VERSION);

  if ParExist('h','help') then begin
    Help;
    Exit;
  end;

  S := 'false';
  Action := LowerCase(Trim(ParamStr(1)));

  hConsoleOut   := GetStdHandle(STD_OUTPUT_HANDLE);
  hConsoleInput := GetStdHandle(STD_INPUT_HANDLE);

  try
    Reg := TRegistry.Create;
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Action = '' then begin
      Reg.OpenKey('SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon',False);
      if Reg.ReadString('AutoAdminLogon') = '1' then begin
        while((S <> 'y') and (S <> 'n') and (S <> 'Y') and (S <> 'N') and (S <> '')) do begin
          Cls;
          Write('Auto logon: ');
          SetAttr(10);
          Write('ENABLED [');
          if Reg.ReadString('DefaultDomainName') <> '' then
            Write(''+Reg.ReadString('DefaultUserName') + '@' + Reg.ReadString('DefaultDomainName'))
          else
            Write(''+Reg.ReadString('DefaultUserName'));
          WriteLn(']');
          SetAttr(7);
          Write('Disable auto logon? [y/N]: ');
          ReadLn(S);
        end;
        if ((S = 'y') or (S = 'Y')) then begin
          DisableAutoLogon;
        end;
      end else begin
        while((S <> 'y') and (S <> 'n') and (S <> 'Y') and (S <> 'N') and (S <> '')) do begin
          Cls;
          Write('Auto logon: ');
          SetAttr(12);
          WriteLn('DISABLED');
          SetAttr(7);
          Write('Enable auto logon? [y/N]: ');
          ReadLn(S);
        end;
        if ((S = 'y') or (S = 'Y')) then begin
          Write('User name: ');
          ReadLn(UserName);
          Write('Passowrd: ');
          UserPassword := ReadPassword;
          Write('Re type passowrd: ');
          ReUserPassword := ReadPassword;
          if UserPassword <> ReUserPassword then begin
            Write('Enable auto logon: ');
            SetAttr(12);
            WriteLn('FALSE [passwords not enc]');
            SetAttr(7);
          end else
            EnableAutoLogin;
        end
      end;
      Write('Press <enter> to continue ...');
      ReadLn;
    end else begin
      if ((Action = '-l') or (Action = '--lock')) then begin
        LockWorkStation;
      end else if Action = 'autolock' then begin
        SetAutoLock(True);
      end else if Action = 'noautolock' then begin
        SetAutoLock(False);
      end else if Action = 'enable' then begin
        UserName     := ParValue('u','user');
        UserPassword := ParValue('p','password');
        if ((UserPassword = '') and (ParExist('p','password'))) then begin
          Write('Passowrd: ');
          UserPassword := ReadPassword;
          Write('Re type passowrd: ');
          ReUserPassword := ReadPassword;
          if UserPassword <> ReUserPassword then begin
            Write('Enable auto login: ');
            SetAttr(12);
            WriteLn('FALSE [passwords not enc]');
            SetAttr(7);
            Exit;
          end
        end;
        EnableAutoLogin;
        if ParExist('sr','smartreboot') then
          SmartReboot;
      end else if Action = 'disable' then begin
        DisableAutoLogon;
        if ParExist('l','lock') then
          LockWorkStation;
      end;
    end;
    Reg.Free;
  except

  end;
end.
